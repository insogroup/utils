package insogroup.utils.utils.FilesDownloader.exceptions;

import java.io.IOException;

public class GenerateFileNameException extends IOException {
    public GenerateFileNameException(String message) {
        super(message);
    }
}
