package insogroup.utils.utils.IOC.exceptions;

import java.io.IOException;

public class ResolveStrategyException extends IOException {

    public ResolveStrategyException(String message, Throwable cause) {
        super(message, cause);
    }

}
